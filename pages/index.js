import React, { Component } from "react";
import Layout from "../layout/Layout";
import { connect } from "react-redux";

class Home extends Component {
  render() {
    return (
      <Layout>
        <section className="div">
          <h1 className="h1">Hello Next Template</h1>
        </section>
      </Layout>
    );
  }
}

const mapStateToProps = state => {
  return {
    // cards: state.userStore.cards
  };
};

const mapDispatchToProps = dispatch => {
  return {
    // addUser: newUser => {
    //   dispatch(userActions.addUserCard(newUser));
    // }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
