import { actionTypes } from "./actionType";

export const updateUser = (uid, field, data) => {
  return {
    type: actionTypes.UPDATE_USER,
    payload: { uid, field, data }
  };
};
// export const addWorkoutCard = workoutCard => {
//   return {
//     type: actionTypes.ADD_WORKOUT,
//     payload: workoutCard
//   };
// };

// export const completeWorkOut = cardId => {
//   return {
//     type: actionTypes.WORKOUT_COMPLETE,
//     payload: cardId
//   };
// };

// export const editWorkout = (cardId, title) => {
//   return {
//     type: actionTypes.WORKOUT_EDIT,
//     payload: { cardId, title }
//   };
// };

// export const removeCard = cardId => {
//   return {
//     type: actionTypes.WORKOUT_REMOVE,
//     payload: cardId
//   };
// };
