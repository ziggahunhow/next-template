import { actionTypes } from "../actions/actionType";
import { initialUserState } from "../models/user";

export const userStore = (state = initialUserState, action = {}) => {
  const payload = action.payload;
  switch (action.type) {
    case actionTypes.UPDATE_USER:
      return {
        user: {
          ...state.user,
          [payload.uid]: {
            ...state.user[payload.uid],
            [payload.field]: payload.data
          }
        }
      };
    default:
      return state;
    // case actionTypes.ADD_WORKOUT:
    //   const randomId = Math.random()
    //     .toString("36")
    //     .slice(8);
    //   return {
    //     cards: {
    //       ...state.cards,
    //       [randomId]: {
    //         workoutDate: new Date().toLocaleDateString("en-US"),
    //         workoutComplete: false,
    //         ...action.payload
    //       }
    //     }
    //   };
    // case actionTypes.WORKOUT_COMPLETE:
    //   return {
    //     cards: {
    //       ...state.cards,
    //       [action.payload]: {
    //         ...state.cards[action.payload],
    //         workoutComplete: !state.cards[action.payload].workoutComplete
    //       }
    //     }
    //   };
    // case actionTypes.WORKOUT_EDIT:
    //   return {
    //     cards: {
    //       ...state.cards,
    //       [action.payload.cardId]: {
    //         ...state.cards[action.payload.cardId],
    //         workoutName: action.payload.title
    //       }
    //     }
    //   };
    // default:
    //   return state;
    // case actionTypes.WORKOUT_REMOVE:
    //   delete state.cards[action.payload];
    //   return {
    //     cards: { ...state.cards }
    //   };
  }
};
