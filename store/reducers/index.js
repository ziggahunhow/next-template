import { combineReducers } from "redux";
import { userStore } from "./user";

export default combineReducers({
  userStore
});
